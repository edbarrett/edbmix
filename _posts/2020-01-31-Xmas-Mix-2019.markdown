---
layout: post
title:  "Xmas Mix 2019"
date:   2019-12-25
categories: mix
---

<script class="podigee-podcast-player" src="https://cdn.podigee.com/podcast-player/javascripts/podigee-podcast-player.js" data-configuration='{"options":{"sslProxy":"https://cdn.podigee.com/ssl-proxy/","startPanels":["Share","Playlist","EpisodeInfo"]},"extensions":{"EpisodeInfo":{"showOnStart":true},"Playlist":{"showOnStart":true},"Share":{"showOnStart":true}},"podcast":{"title":"edbmix","feed":"https://example.com/feed.xml"},"episode":{"media":{"mp3":"https://edbmix.s3-ap-southeast-2.amazonaws.com/mp3/Xmas+Mix+2019.mp3"},"coverUrl":"https://edbmix.s3-ap-southeast-2.amazonaws.com/jpg/Xmas+Mix+2019.jpg","title":"edb - Xmas Mix 2019"}}'></script>
