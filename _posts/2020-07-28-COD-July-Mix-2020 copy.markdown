---
layout: post
title:  "Guest Mix - Throwback Tuesday with DJ cOdfather - July 2020"
date:   2020-07-28
categories: mix
---

<script class="podigee-podcast-player" src="https://cdn.podigee.com/podcast-player/javascripts/podigee-podcast-player.js" data-configuration='{"options":{"sslProxy":"https://cdn.podigee.com/ssl-proxy/","startPanels":["Share","Playlist","EpisodeInfo"]},"extensions":{"EpisodeInfo":{"showOnStart":true},"Playlist":{"showOnStart":true},"Share":{"showOnStart":true}},"podcast":{"title":"edbmix","feed":"https://example.com/feed.xml"},"episode":{"media":{"mp3":"https://edbmix.s3-ap-southeast-2.amazonaws.com/mp3/Cod+July+2020.mp3"},"coverUrl":"https://edbmix.s3-ap-southeast-2.amazonaws.com/jpg/Cod+July+2020.png","title":"Guest Mix - Throwback Tuesday with DJ cOdfather - July 2020"}}'></script>
